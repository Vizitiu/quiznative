const tintColor = '#2f95dc';

export default {
  tintColor,
  tabIconDefault: '#ccc',
  tabIconBlack: '#262626',
  tabIconSelected: tintColor,
  defaultColor: '#fff',
  mainColor: '#03A9F4',
  bgcColor: '#282828',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
