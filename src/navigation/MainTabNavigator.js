import React from 'react';
import {Platform} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import TabBarIcon from '../components/ui/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import QuizesScreen from '../screens/QuizesScreen';
import {QuizScreen} from "../screens/QuizScreen";

const config = Platform.select({
    web: {headerMode: 'screen'},
    default: {},
});

const HomeStack = createStackNavigator(
    {
        Home: HomeScreen,
    },
    config
);

HomeStack.navigationOptions = {
    tabBarLabel: 'Главная',
    tabBarIcon: ({focused}) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-home${focused ? '' : '-outline'}`
                    : 'md-home'
            }
        />
    ),
};

HomeStack.path = '';

const QuizesStack = createStackNavigator(
    {
        Quizes: QuizesScreen,
        Quiz: QuizScreen
    },
    {
        defaultNavigationOptions: {
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: '#03a9f4'
            }
        }
    }
);

QuizesStack.navigationOptions = {
    tabBarLabel: 'Тесты',
    tabBarIcon: ({focused}) => (
        <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-grid' : 'md-grid'}/>
    )
};

QuizesStack.path = '';


const tabNavigator = createBottomTabNavigator({
        HomeStack,
        QuizesStack,
    },
);

tabNavigator.path = '';

export default tabNavigator;
