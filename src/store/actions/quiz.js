import {
    FETCH_QUIZES,
    GET_QUIZ,
    NEXT_QUIZ,
    RESET_FINISHED,
    SET_ERROR,
    SET_FINISHED,
    SET_LOADING
} from "../../types";
import axios from 'axios'

export const fetchQuizes = () => {
    return async dispatch => {
        dispatch(setLoading())

        try {
            const res = await axios.get('https://quizhooks.firebaseio.com/quiz.json')
            const data = res.data
            dispatch(getQuizes(data))
        } catch (e) {
            dispatch(setError())
        }
    }
}

export const fetchQuiz = quizId => {
    return async dispatch => {
        dispatch(setLoading())

        try {
            const res = await axios.get(`https://quizhooks.firebaseio.com/quiz/${quizId}.json`)
            const quiz = res.data
            dispatch(getQuiz(quiz))
        } catch (e) {
            dispatch(setError())
        }
    }
}

export const setNextQuiz = id => {
    return async (dispatch, getState) => {
        try {
            const state = getState().quiz
            const quizActive = state.quizActive
            const results = state.results
            const quizLength = state.quiz.questions.length
            const rightQuiz = state.quiz.questions[quizActive].rightQuiz

            const timeout = setTimeout(() => {
                if (id === rightQuiz) {
                    results[quizActive] = 'true'
                } else results[quizActive] = 'false'

                if (quizLength !== quizActive + 1) {
                    dispatch(getNextQuiz(quizActive + 1))
                } else dispatch(setFinished())
                clearTimeout(timeout)
            }, 500)


        } catch (e) {
            dispatch(setError())
        }
    }
}

export const getQuizes = quizes => ({
    type: FETCH_QUIZES,
    quizes
})

export const getNextQuiz = quizActive => ({
    type: NEXT_QUIZ,
    quizActive
})

export const getQuiz = quiz => ({
    type: GET_QUIZ,
    quiz
})

export const setLoading = () => ({type: SET_LOADING})
export const setError = () => ({type: SET_ERROR})
export const setFinished = () => ({type: SET_FINISHED})
export const resetFinished = () => ({type: RESET_FINISHED})
