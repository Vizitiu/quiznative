import {
    FETCH_QUIZES,
    GET_QUIZ,
    NEXT_QUIZ,
    RESET_FINISHED,
    SET_ERROR,
    SET_FINISHED,
    SET_LOADING
} from "../../types";

const STATE = {
    quizes: [],
    quiz: null,
    quizActive: 0,
    results: [],
    loading: false,
    error: false,
    finished: false
}

export const quiz = (state = STATE, {type, quizes, quiz, msg, quizActive}) => {
    switch (type) {
        case SET_LOADING:
            return {...state, loading: true, error: false}
        case SET_ERROR:
            return {...state, loading: false, error: true}
        case FETCH_QUIZES:
            return {...state, quizes, loading: false, error: false}
        case GET_QUIZ:
            return {...state, quiz, loading: false, error: false, finished: false, results: [], quizActive: 0}
        case NEXT_QUIZ:
            return {...state, quizActive, loading: false, error: false}
        case SET_FINISHED:
            return {...state, finished: true, loading: false, error: false, quizActive: 0}
        case RESET_FINISHED:
            return {...state, finished: false, results: [], error: false, loading: false}
        default:
            return state
    }
}
