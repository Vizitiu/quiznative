import React from 'react';
import {TouchableOpacity, View, StyleSheet, ImageBackground, Text} from 'react-native'
import PropTypes from 'prop-types'

export const Quizes = ({handleOpen, id, img, title}) => {

    return (
        <TouchableOpacity activeOpacity={0.5} onPress={() => handleOpen(id, title)}>
            <View style={styles.quizes}>
                <ImageBackground style={styles.images}
                                 source={{uri: img}}>
                    <Text style={styles.text}>{title}</Text>
                </ImageBackground>
            </View>
        </TouchableOpacity>
    );
};

Quizes.propTypes = {
    handleOpen: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
    quizes: {
        width: '100%',
        marginBottom: 20,
    },
    images: {
        width: '100%',
        height: 200,
    },
    text: {
        color: '#fff',
        backgroundColor: 'rgba(35,35,35,0.67)',
        fontSize: 30,
        margin: 10,
        padding: 5,
        textAlign: 'center',
        marginTop: 50,
        fontWeight: '700'
    }
})
