import React from 'react';
import {View, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
import {QuizItem} from "./QuizItem/QuizItem";
import {MonoText} from "../ui/StyledText";

export const Quiz = ({question, answers, nextQuiz, quizActive}) => {

    return (
        <View style={styles.quiz}>
            <MonoText style={styles.quizTitle}>{question}</MonoText>
            <View style={styles.quizWrapper}>
                {answers.map(({id, title}) => (
                    <QuizItem id={id}
                              key={id}
                              title={title}
                              nextQuiz={nextQuiz}
                    />
                ))}
            </View>
            <MonoText style={styles.activeQuiz}>Вопрос: {quizActive + 1}</MonoText>
        </View>
    );
};

Quiz.propTypes = {
    question: PropTypes.string,
    answers: PropTypes.array.isRequired,
    nextQuiz: PropTypes.func.isRequired,
    quizActive: PropTypes.number.isRequired
};

const styles = StyleSheet.create({
    quiz: {
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    quizTitle: {
        fontSize: 26,
        fontWeight: '700'
    },
    quizWrapper: {
        backgroundColor: '#00537b',
        marginTop: 30,
    },
    activeQuiz: {
        fontSize: 25,
        marginTop: 10
    }
})
