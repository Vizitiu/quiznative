import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types'

export const QuizItem = ({id, title, nextQuiz}) => {
    return (
            <TouchableOpacity key={id} onPress={() => nextQuiz(id)} style={styles.item}>
                <Text>{title}</Text>
            </TouchableOpacity>
    );
};

QuizItem.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    nextQuiz: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        paddingVertical: 30,
        paddingHorizontal: 100,
        borderColor: '#000',
        borderWidth: .5
    }
})
