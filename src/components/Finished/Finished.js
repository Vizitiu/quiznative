import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native'
import PropTypes from 'prop-types'
import {MonoText} from "../ui/StyledText";
import {FinishedItem} from "./FinishedItem/FinishedItem";
import {MaterialCommunityIcons} from '@expo/vector-icons'

export const Finished = ({questions, goToQuizes, results}) => {

    const rightAnswers = Object.keys(results).reduce((total, key) => {
        if (results[key] === 'true') total++
        return total
    }, 0)

    return (
        <View style={styles.finished}>
            <MonoText style={styles.title}>Опрос окончен</MonoText>
            <ScrollView style={styles.finishedItem}>
                {questions.map(({question, id}) => (
                    <FinishedItem key={id} title={question} results={results} id={id}/>
                ))}
            </ScrollView>
            <MonoText style={styles.rightAnswers}>Правильные ответы {rightAnswers} из 4</MonoText>
            <MaterialCommunityIcons.Button name={'arrow-left-bold-outline'}
                              onPress={goToQuizes}>
                Перейти к тестам
            </MaterialCommunityIcons.Button>
        </View>
    );
};

Finished.propTypes = {
    questions: PropTypes.array.isRequired,
    goToQuizes: PropTypes.func.isRequired,
    results: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
    finished: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    title: {
        fontSize: 35,
    },
    finishedItem: {
        marginHorizontal: 10,
        marginVertical: 30,
    },
    rightAnswers: {
        fontSize: 20,
        marginVertical: 10
    }
})
