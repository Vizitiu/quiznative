import React from 'react';
import {View, StyleSheet, Text} from 'react-native'
import PropTypes from 'prop-types'
import {AntDesign} from "@expo/vector-icons";

export const FinishedItem = ({title, results, id}) => {
    return (
        <View style={styles.items}>
            <Text style={styles.text}>
                {title}
            </Text>
            {results[id] === 'true'
                ? <AntDesign name={'checkcircle'}
                             style={[styles.icon, {color: 'green'}]}
                />
                : <AntDesign name={'closecircle'}
                                          style={[styles.icon, {color: 'red'}]}
                />
            }
        </View>
    );
};

FinishedItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    results: PropTypes.array.isRequired
};

const styles = StyleSheet.create({
    items: {
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        padding: 10,
        backgroundColor: '#161616',
        borderBottomColor: '#4b39ff',
        borderWidth: .5
    },
    text: {
        fontSize: 21,
        color: '#fff',
        fontWeight: '700'
    },
    icon: {
        fontSize: 24,
    }
})
