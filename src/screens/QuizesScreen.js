import React, {useEffect} from 'react';
import {StyleSheet, SafeAreaView, FlatList, ActivityIndicator, View, Text} from 'react-native';
import {Quizes} from "../components/Quizes/Quizes";
import Colors from "../../constants/Colors";
import {useDispatch, useSelector} from "react-redux";
import {fetchQuizes} from "../store/actions/quiz";

function useRedux() {
    const dispatch = useDispatch()
    const quizes = useSelector(state => state.quiz.quizes)
    const loading = useSelector(state => state.quiz.loading)
    const error = useSelector(state => state.quiz.error)

    return {
        quizes, dispatch, loading, error
    }
}

export default function QuizesScreen({navigation}) {

    const {quizes, dispatch, loading, error} = useRedux()

    useEffect(() => {
        dispatch(fetchQuizes())
    }, [fetchQuizes])


    const handleOpen = (id, title) => {
        navigation.navigate('Quiz', {quizId: id, title})
    }

    return (
        <SafeAreaView style={styles.container}>
            {error && <View style={styles.center}><Text style={styles.errorText}>Что-то пошло не так!</Text></View>}
            {loading
                ? <View style={styles.center}><ActivityIndicator size="large" color="#0000ff" /></View>
                : <FlatList
                    data={quizes}
                    renderItem={({item}) =>
                        <Quizes id={item.id}
                                img={item.img}
                                title={item.title}
                                handleOpen={handleOpen}
                        />}
                    keyExtractor={item => item.id.toString()}
                />
            }
        </SafeAreaView>
    );
}

QuizesScreen.navigationOptions = {
    title: 'Тесты',
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35,
        backgroundColor: Colors.bgcColor
    },
    center: {
        flex: 1,
        justifyContent: 'center',
    },
    errorText: {
        fontSize: 50,
        color: Colors.errorText
    }
});
