import React from 'react';
import {StyleSheet, View} from 'react-native';
import {MonoText} from '../components/ui/StyledText'
import Colors from "../../constants/Colors";


export default function HomeScreen() {
    return (
        <View style={styles.container}>
            <MonoText style={styles.title}>Home Page</MonoText>
        </View>
    );
}

HomeScreen.navigationOptions = {
    header: null,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.bgcColor
    },
    title: {
        color: '#fff',
        fontSize: 23
    }
});
