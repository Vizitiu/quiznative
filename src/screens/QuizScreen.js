import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ActivityIndicator, Text} from 'react-native'
import {useDispatch, useSelector} from "react-redux";
import {fetchQuiz, setNextQuiz, resetFinished} from "../store/actions/quiz";
import {Quiz} from "../components/Quiz/Quiz";
import {Finished} from "../components/Finished/Finished";
import Colors from "../../constants/Colors";

const useRedux = () => {
    const dispatch = useDispatch()
    const quiz = useSelector(state => state.quiz.quiz)
    const quizActive = useSelector(state => state.quiz.quizActive)
    const finished = useSelector(state => state.quiz.finished)
    const results = useSelector(state => state.quiz.results)
    const loading = useSelector(state => state.quiz.loading)

    return {
        dispatch, quiz, quizActive, finished, results, loading
    }
}

export const QuizScreen = ({navigation}) => {
    const {quiz, dispatch, quizActive, finished, results, loading} = useRedux()
    const quizId = navigation.getParam('quizId')

    useEffect(() => {
        dispatch(fetchQuiz(quizId))
    }, [fetchQuiz]);

    const nextQuiz = id => {
        dispatch(setNextQuiz(id))
    }

    const goToQuizes = () => {
        navigation.popToTop('Quizes')
        dispatch(resetFinished())
    }

    if (loading) return <View style={styles.center}><ActivityIndicator/></View>

    return (
        <View style={styles.container}>
            {finished
                ? <Finished questions={quiz.questions}
                            goToQuizes={goToQuizes}
                            results={results}
                />
                : quiz === null
                    ? <ActivityIndicator/>
                    : <Quiz question={quiz.questions[quizActive].question}
                            answers={quiz.questions[quizActive].answers}
                            nextQuiz={nextQuiz}
                            quizActive={quizActive}
                    />
            }
        </View>
    );
};

QuizScreen.navigationOptions = ({navigation}) => ({
    headerTitle: `${navigation.getParam('title')}`
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.bgcColor
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.bgcColor
    }
})
